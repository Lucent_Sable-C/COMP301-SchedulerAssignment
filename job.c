#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include "job.h"
//print all loaded jobs to stdout
void print_jobs(JOB* head)
{
	printf("PID\tBurst\twait\n");
	while(head!=NULL)
	{
		printf("%d\t%d\t%d\n",head->pid,head->burst,head->wait);
		head = head->next;
	}
}

//adds a job after list_item to the list
//	the new job has a burst length of BURST
//	the new job has a pid of PID
//returns a pointer to the new job
JOB* add_job(JOB* prev, int PID, int BURST)
{
	JOB* out;
	out = malloc(sizeof(JOB));
	
	if(prev == NULL)
	//if the current job is null (not exist) then cerate a new job
	{
		//first item in list has no previous item
		out->prev  = NULL;
		out->pid   = PID;
		out->burst = BURST;
		out->wait  = 0;
		//last item in list has no next item
		out->next  = NULL;
	}
	else //add after the item passed in
	{
		//get the next job in the list
		JOB* next = prev->next;
		//the new job will be inserted between prev and next
		out->prev  = prev;
		out->pid   = PID;
		out->burst = BURST;
		out->wait  = 0;
		out->next  = next;
		
		//update the jobs either side of the new job
		prev->next = out;
		if(next!=NULL)
			next->prev = out;
	}
	
	return out;
	
}

JOB* insert_job(JOB* element, JOB* list)
{
	//add the job after the element in the specified list
	return add_job(list, element->pid, element->burst);
}

//removes a job from the list
JOB* remove_job(JOB* item)
{
	JOB* prev = item->prev;
	JOB* next = item->next;
	
	//free the memory allocated with the job
	free(item);
	
	//link the next and previous jobs together
	if(prev!=NULL)
		prev->next = next;
	if(next!=NULL)
		next->prev = prev;
	
	if(prev!=NULL)return prev;
	return next;
}

//removes all jobs after head in the list
void clear_list(JOB* head)
{
	while(head!=NULL)
	{
		JOB* next = head->next;
		remove_job(head);
		head = next;
	}
}

JOB* seek_head(JOB* element)
{
	if(element==NULL)return NULL;
		//traverse to the head of the sorted list
	while(element->prev != NULL)element=element->prev;
	return element;
}

JOB* sort_list(JOB* head)
{
	//list to store the sorted elements
	JOB* sort_head = NULL;
	int val_short;
	JOB* ptr_short;
	JOB* ptr_curr;
	while(head!=NULL)
	{
		val_short = INT_MAX;
		ptr_short = NULL;
		ptr_curr = head;
		//search the origional list for the smallest burst length
		while(ptr_curr != NULL)
		{
			if(ptr_curr->burst<val_short)
			{
				val_short = ptr_curr->burst;
				ptr_short = ptr_curr;
			}
			ptr_curr = ptr_curr->next;
		}
		//add the found element to the end of the list
		sort_head = insert_job(ptr_short,sort_head);
		//removethe sorted element from the origional list
		head = seek_head(remove_job(ptr_short));
	}
	//at this point, the origional list has been freed completely
	//and thre sorted list is fully populated
	sort_head = seek_head(sort_head);
	
	return sort_head;
}