#define JOB struct job

JOB
{
		JOB* prev; //pointer to previous job in list
		int pid;   //the process id of this job
		int burst; //the burst length requested
		int wait;  //the time this job had to wait before starting
		JOB* next; //pointer to the next job
};

extern void print_jobs(JOB* head);
extern JOB* add_job(JOB* prev, int PID, int BURST);
extern JOB* insert_job(JOB* element, JOB* list);
extern JOB* remove_job(JOB* item);
extern void clear_list(JOB* head);
extern JOB* seek_head(JOB* element);
extern JOB* sort_list(JOB* head);

