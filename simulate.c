/*Ensure that the files job.c and job.h exist in the same directory, and compile with
	gcc -Wall simulate.c job.c -o simulate.out*/

#include <stdio.h>
#include <getopt.h>
#include "job.h"

JOB* read_file(char* file)
{
	char bin;
	JOB* list_head = NULL;
	int i = NULL;
	int b = NULL;
	//open the file
	FILE* reader;
	reader = fopen(file, "r");
	//if the file opened successfully
	if(reader!=NULL)
	{
		//read the first line, to get the i,bfrom the input file
		//fscanf reading two characters means the file format is likely correct
		if(fscanf(reader, "%c,%c",&bin,&bin)==2)
		{
			//read lines from the file
			while(fscanf(reader, "%d,%d", &i, &b)==2)
			{
				list_head = add_job(list_head, i, b);
			}			
		}
		//close the file
		fclose(reader);
		
		return seek_head(list_head);
	}
	return NULL;
}

int calculate_wait(JOB* head)
{
	int wait = 0;
	
	//iterate through whole list
	while(head != NULL)
	{
		head->wait = wait;
		wait+=head->burst;
		head = head->next;
	}
	
	return wait;
}

double avg_wait(JOB* head)
{
	int total = 0;
	int count = 0;
	
	while(head!=NULL)
	{
		total+=head->wait;
		count++;
		head=head->next;
	}
	
	return (double)total/(double)count;
}

int main(int argc, char *argv[]) {
	//data structure for long arguments
	static struct option long_options[] = {
		{"FILE"    ,required_argument,NULL,1},
		{"PROC"    ,required_argument,NULL,2},
		{NULL      ,0                ,NULL,0}
	};
    int option = 0;
	int option_index = 0;
	
	char* file;
	char* proc;
    JOB* list = NULL;

    //Specifying the expected options
    //The two options l and b expect numbers as argument
    while ((option = getopt_long_only(argc, argv,"wbf", long_options, &option_index)) != -1)
	{
        switch (option) {
            case 1: //FILE
				file = optarg;
				break;
			case 2: //PROC
				proc = optarg;
				break;
			default: //default case, do nothing
				break;
        }
    }
	
	//exit the program if the arguments were not passed
	if(proc==NULL||file==NULL){printf("Missing Argument");return 1;};
	
	//read the file, create a list and store the pointer to the first element in the list
	list = read_file(file);
	
	//the two options are "S" or "F" which stand for Shortest first or First come.
	switch (proc[0])
	{
		case 'F':
		case 'f':
			printf("First Come First Served: %s\n", file);
			//no sorting needed
			break;
		case 'S':
		case 's':
			printf("Shortest Job First: %s\n", file);
			list = sort_list(list);
			break;
		default:
			printf("Please provide either 'F' or 'S' as PROC options\n");
			return 1;
	}
	
	//at this point, the list should be read and sorted, ready for processing and outputting
	
	//now iterate though, updating the wait times
	calculate_wait(list);
	//all wait times are updated
	//print out the job list
	print_jobs(list);
	
	printf("Average waiting time:%f\n",avg_wait(list));
	
	//free all memory used by the program
	list = seek_head(list);
	clear_list(list);
    return 0;
}